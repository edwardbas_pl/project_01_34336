#include <iostream>
#include <string>
using namespace std;

//Funkcje zaimplementował Marcin Warzała
int checksum_check( int array[] ){
	int sum;
	int checksum;
	int a,b,c,d,e,f,g,h,i,j;
	
	a = 0 * array[0];
	if( a > 9 ){
		a = a%10;
	}

	b = 2 * array[1];
	if( b > 9 ){
		b = b%10;
	}

	c = 0 * array[2];
	if( c > 9 ){
		c = c%10;
	}
	
	d = 7 * array[3];
	if( d > 9 ){
		d = d%10;
	}
	
	e = 0 * array[4];
	if( e > 9 ){
		e = e%10;
	}
	
	f = 8 * array[5];
	if( f > 9 ){
		f = f%10;
	}
	
	g = 0 * array[6];
	if( g > 9 ){
		g = g%10;
	}
	
	h = 3 * array[7];
	if( h > 9 ){
		h = h%10;
	}
	
	i = 6 * array[8];
	if( i > 9 ){
		i = i%10;
	}
	j = 2 * array[9];
	if( j > 9 ){
		j = j%10;
	}
	sum = a+b+c+d+e+f+g+h+i+j;
	//cout << sum << endl;
	if( sum > 9 ){
		sum = sum%10;
	}
	checksum = 10 - sum;
	if( checksum == array[10] ){
		cout << "pesel poprawny" << endl;
		return 1;
	}
	else {
		return 0;
	}
	return 0;

}



//Funkcje zaimplementował Marcin Warzała
void parse_to_int_array( string input  , int array[] ){

	for( int i = (input.length()-1) ; i >= 0 ; i-- ){
		array[i] = input.at(i) - '0' ;
		//cout << pesel.at(i) - '0' << endl;
	}

}

//Funkcje zaimplementował Marcin Warzała
bool isNumber(const string& str)
{
    for (char const &c : str) {
        if (std::isdigit(c) == 0) return false;
    }
    return true;
}

//Funkcje zaimplementował Marcin Warzała
int gender( int arr[] ){
	if( arr[9]%2 == 0 ){
		cout << "Właściciel jest Kobietą" << endl;
		return 0;
	}
	else{
		cout << "Właściciel Jest Mężczyzną" << endl;
		return 1;
	}
	return -1;
}

//Funkcje zaimplementował Alber Najdała

int daylife( int day[] ){
	int day_life = 10 * day[4];
	day_life += day[5];
    
	
    return day_life;
}

//Funkcje zaimplementował Alber Najdała

int monthlife( int month[] ){
	
	int month_life = 10 * month[2] ;
	month_life += month[3] ;
	
    if( month_life > 80 && month_life < 93 ) {
        month_life -= 80;
    }
    else if( month_life > 20 && month_life < 33 ) {
        month_life -= 20;
    }
    else if( month_life > 40 && month_life < 53 ) {
        month_life -= 40;
    }
    else if( month_life > 60 && month_life < 73 ) {
        month_life -= 60;
    }
    return month_life;
}

//Funkcje zaimplementował Alber Najdała

int yearlife( int year[] ){
    
    int year_life = 10 * year[ 0 ];
    year_life += year[ 1 ];
    
    int month_life = 10 * year[ 2 ];
    month_life += year[ 3 ];
    
    if( month_life > 80 && month_life < 93 ) {
        year_life += 1800;
    }
    else if( month_life > 0 && month_life < 13 ) {
        year_life += 1900;
    }
    else if( month_life > 20 && month_life < 33 ) {
        year_life += 2000;
    }
    else if( month_life > 40 && month_life < 53 ) {
        year_life += 2100;
    }
    else if( month_life > 60 && month_life < 73 ) {
        year_life += 2200;
    }
    return year_life;
}
int main( int argc , char** argv ){

  	string pesel;
	int pesel_array[11];
	pesel = argv[1];
	//string pesel = ("00240903853");
	
	if( pesel.empty()  ){
		cout << "PESEL nie podany... Wychodzę..." << endl;
	}

	if( isNumber(pesel) == true ){
		cout << "PESEL jest liczbą\n";
	}
	else{
 		cout << "Podany argument nie jest liczbą... Wychodzę...\n";
		return -1;
	}

	if( pesel.length() != 11 ){
		cout << "podany pesel nie jest wymaganej długości... Wychodzę... " << endl;
		return -1;
	}

	parse_to_int_array( pesel , pesel_array );
	//cout << checksum_check( pesel_array ) << endl;
	checksum_check( pesel_array );
	gender( pesel_array );
	
	if(daylife(pesel_array) < 30  && monthlife(pesel_array)== 2 && (yearlife(pesel_array)%400==0 ||yearlife(pesel_array)%4==0 && yearlife(pesel_array)!=0 )){
	    
	    cout <<  "Dzien urodzenia: "<< daylife(pesel_array) << endl;
	    cout <<  "Miesiąc urodzenia: "<< monthlife(pesel_array) << endl;
	    cout <<  "Rok urodzenia przestępny: "<< yearlife(pesel_array) << endl;
	}
	
	else if ( daylife(pesel_array) < 32 && monthlife(pesel_array)< 13 && monthlife(pesel_array)!=2 ){
	    
	    cout <<  "Dzien urodzenia: "<< daylife(pesel_array) << endl;
	    cout <<  "Miesiąc urodzenia: "<< monthlife(pesel_array) << endl;
	    cout <<  "Rok urodzenia nie przestępny: "<< yearlife(pesel_array) << endl;
	}    
	else if (daylife(pesel_array) < 29 && monthlife(pesel_array)== 2){
	    
	    cout <<  "Dzien urodzenia: "<< daylife(pesel_array) << endl;
	    cout <<  "Miesiąc urodzenia: "<< monthlife(pesel_array) << endl;
	    cout <<  "Rok urodzenia nie przestępny: "<< yearlife(pesel_array) << endl;
	}
	
    else {
        cout << "Nie poprawny format " << endl ;
        exit(0);
    }
    
    
    if (daylife(pesel_array)<= 9  && monthlife(pesel_array) <=9){
        
        cout <<  "Data urodzenia: 0"<< daylife(pesel_array)<< "-0"<< monthlife(pesel_array)<< "-"<< yearlife(pesel_array) << endl;
    }
    else if (daylife(pesel_array)<= 9 && monthlife(pesel_array) >=9){
        
        cout <<  "Data urodzenia: 0"<< daylife(pesel_array)<< "-"<< monthlife(pesel_array)<< "-"<< yearlife(pesel_array) << endl;
    }
    else if (daylife(pesel_array)>= 9 && monthlife(pesel_array) <=9){
        
        cout <<  "Data urodzenia: "<< daylife(pesel_array)<< "-0"<< monthlife(pesel_array)<< "-"<< yearlife(pesel_array) << endl;
    }
    else  {
        
        cout <<  "Data urodzenia: "<< daylife(pesel_array)<< "-"<< monthlife(pesel_array)<< "-"<< yearlife(pesel_array) << endl;
    }
	return 0;
}
