﻿### Instukcja Kompilacji ###
W pobranym folderze należy wywłoać nastepującą komendę:
<code> g++ main.cpp -o pesel </code>

### Instrukcja Obsługi ###
Program jest uruchamiany z konsoli.

W przypadku uruchamiania na programu w systemie Linux, program wywływany jest w następujący sposób:

<code> ./pesel <*numer_pesel*> </code>


Program nie komunikuje sie z użytkownikiem w żaden spoób. jedynie wypisuje w konsoli na wyjściu wyczytane z podanego jako argument numeru pesel informacje. Po potiwerdzeniu czy jest on poprawny.

Przykładowe wywołanie programu wraz z informacją zwrotną od programu:

*Wywołanie:*

<code> $ ./pesel 00240903853 </code>


*Wyjście*

<code>PESEL jest liczbą</code>

<code>pesel poprawny</code>

<code>Właściciel Jest Mężczyzną</code>

<code>Dzien urodzenia: 9</code>

<code>Miesiąc urodzenia: 4</code>

<code>Rok urodzenia nie przestępny: 2000</code>

<code>Data urodzenia: 09-04-2000</code>



W przeciwnym wypadku (Podany pesel jest za długi / krótki, lub zawiera litery). Program na wyjściu wyświetla komunikat o niepoprawnym numeru pesel i kończy działanie.


Przykładowe wywołanie programu wraz z informacją zwrotną od programu w przypadku pdania niepoprawnych numerów pesel:

*Wywołanie (podany PESEL ma długośc 10 znaków):*

<code>$ ./pesel 0024090385 </code>

*Wyjście:*

<code>PESEL jest liczbą </code>

<code>podany pesel nie jest wymaganej długości... Wychodzę... </code>

*Wywołanie (podany PESEL zawiera literę):*

<code> $ ./pesel 002409038n3 </code>

*Wyjście:*

<code> Podany argument nie jest liczbą... Wychodzę... </code>

*Wywołanie (podany PESEL ma długość 12 znaków):

<code> $ ./pesel 002409038537 </code>

*Wyjście:*

<code>PESEL jest liczbą</code>

<code>podany pesel nie jest wymaganej długości... Wychodzę... </code>

### Wykorzystane Narzędzia ###
###  Marcin Warzała:
System Operacyjny: *Arch Linux*
Edytor Kodu: *Vim*

### Albert Najdała:
System Operacyjny: *Windows 10*
Edytor Kodu: *Codeblocks IDE*

## Tabela Podziału Zadań:

|			Marcin Warzała			|			Albrt Najdała			|
|-------------------------|:---------------------:|
|Sprawdzanie sumy kontrolnej |  Dzień urodzenia   |
|Funkcja 	parsująca  ciag znaków do tablicy Inów	| Miesiąc Urodzenia
|         sprawdzanie czy w ciagu znakow znajduja sie wyłacznie cyfry                | Rok Urodzenia                      |
|    sprawdzanie płci                     |    Zabezpieczenia przed niemożliwą datą                   |


### Problemy napotkane w trakcie Tworzenia projektu ###

*  przed zaimplementowaniem zabezpieczenia przed podaniem nr PESEL zawierajacego rok ponad zakresem wyswietlała sie wyłącznie liczba z numeru pesel podajaca rok. zamiast pełnego zapisu roku. Powodowało to że możana było podać niepoprawny numer pesel ktory przechodzi sprawdzanie sumy kontrolnej.
* możliwe było podanie numeru pesel ktory przechodził pomyślnie sprawdzanie sumy kontrolnej, jednak molwie było podanie dnia miesiaca ktory nie ma prawa wystąpić. Np. 32 Lutego.
* Chcąc zaimplementować obsułge wywołania programu dla nie podania numeru pesel przy wywołaniu programu tak aby program sam spytał sie o jego padanie. Występował bład segmentacji. Tego problmu nie udało się nam rozwiązać.
